package comsoc

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	//err = "test"
	count = make(Count)
	for i, v := range p {
		for j := 0; j < thresholds[i]; j++ {
			count[v[j]]++
		}
	}
	return
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	var c Count
	c, err = ApprovalSWF(p, thresholds)
	if err != nil {

	}
	bestAlts = maxCount(c)
	return
}
