package agt

import cs "gitlab.utc.fr/porteede/ia04-td3-porte-hericourt/comsoc"

type AgentI interface {
	Start()
}

type RequestNewBallot struct {
	Rule     string           `json:"rule"`
	Deadline string           `json:"deadline"`
	VoterIds []string         `json:"voter-ids"`
	Alts     int              `json:"#alts"`
	Tiebreak []cs.Alternative `json:"tiebreak"`
}

type RequestVote struct {
	VoterId  string           `json:"agent-id"`
	BallotId string           `json:"vote-id"`
	Prefs    []cs.Alternative `json:"prefs"`
	Options  []int            `json:"options"`
}

type RequestResult struct {
	BallotId string `json:"ballot-id"`
}

type ResponseNewBallot struct {
	BallotId string `json:"ballot-id"`
}

type ResponseResult struct {
	Winner  cs.Alternative   `json:"winner"`
	Ranking []cs.Alternative `json:"ranking"`
}
