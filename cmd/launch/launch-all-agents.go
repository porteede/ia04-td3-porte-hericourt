package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"strconv"
	"time"

	"gitlab.utc.fr/porteede/ia04-td3-porte-hericourt/agt"
	cs "gitlab.utc.fr/porteede/ia04-td3-porte-hericourt/comsoc"
)

func main() {
	const url1 = ":8080"
	const url2 = "http://localhost:8080"

	var n int = 10
	var m int = 100

	// lecture des paramètres entrés par l'utilisateur
	if len(os.Args) > 1 {
		voter_number_str := os.Args[1]
		voter_number, err1 := strconv.Atoi(voter_number_str)
		if err1 != nil {
			panic(fmt.Sprintf("Nombre de votants incorrect : %s", voter_number_str))
		} else {
			n = voter_number
		}

		if len(os.Args) > 2 {
			max_alternatives_str := os.Args[2]
			max_alternatives, err2 := strconv.Atoi(max_alternatives_str)
			if err2 != nil {
				panic(fmt.Sprintf("Nombre d'alternatives maximum incorrect : %s", max_alternatives_str))
			} else {
				m = max_alternatives
			}
		}
	}

	channel := make(chan string)
	alts := make([]cs.Alternative, 0)
	for i := 1; i <= m; i++ {
		alts = append(alts, cs.Alternative(i))
	}

	clientAgents := make([]agt.VoterAgent, 0, n)
	serverAgent := agt.NewBallotAgent("ballot-agent", url1)

	log.Println("démarrage du serveur...")
	go serverAgent.Start()

	log.Println("démarrage des clients...")
	for i := 0; i < n; i++ {
		id := fmt.Sprintf("id%02d", i)
		prefs := make([]cs.Alternative, len(alts))
		copy(prefs, alts[:])
		rand.Shuffle(len(prefs), func(i, j int) { prefs[i], prefs[j] = prefs[j], prefs[i] })
		var thresholds []int
		thresholds = append(thresholds, 2)
		voter := agt.NewVoterAgent(id, prefs, url2, channel, thresholds)
		clientAgents = append(clientAgents, *voter)
	}

	for _, voter := range clientAgents {
		// attention, obligation de passer par cette lambda pour faire capturer la valeur de l'itération par la goroutine
		func(voter agt.VoterAgent) {
			go voter.Start()
		}(voter)
	}

	var input string
	for {
		time.Sleep(time.Second)
		fmt.Print("Indiquez l'identifiant d'un ballot pour lancer un vote (\"stop\" pour quitter) :\n")
		fmt.Scanln(&input)
		if input == "stop" {
			return
		}
		for i := 0; i < n; i++ {
			channel <- input
		}
	}
}
