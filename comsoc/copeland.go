package comsoc

func CopelandSWF(p Profile) (count Count, err error) {
	count = make(Count)
	err = nil
	var ci, cj int            // compteurs pour les alternatives i et j
	for _, v1 := range p[0] { // Pour chaque alternative
		for _, v2 := range p[0] { // Pour chaque alternative
			if v1 != v2 {
				ci = 0
				cj = 0
				for _, v3 := range p {
					if isPref(Alternative(v1), Alternative(v2), v3) {
						ci++
					} else {
						cj++
					}
				}
				if ci > cj {
					count[Alternative(v1)]++
				} else if ci < cj {
					count[Alternative(v1)]--
				} // 0 en cas d'égalité
			}
		}
	}
	return
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	var c Count
	c, err = CopelandSWF(p)
	if err != nil {

	}
	bestAlts = maxCount(c)
	return
}
