package comsoc

import (
	"errors"
)

type Alternative int           // candidats 1,2,3 et 4
type Profile [][]Alternative   // 0 : [2,4,3,1],  1 : [4,2,3,1] etc...
type Count map[Alternative]int // map[0:37 1:48 2:47 3:30] -> candidat 1 gagne

// renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i, v := range prefs {
		if alt == v {
			return i
		}
	}
	return -1 // error
}

// renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	return rank(alt1, prefs) < rank(alt2, prefs)
}

// renvoie les meilleures alternatives pour un décompte donné
func maxCount(count Count) (bestAlts []Alternative) {
	max := 0
	for key, v := range count {
		if v > max {
			max = v
			bestAlts = make([]Alternative, 0) // On remets à 0
			bestAlts = append(bestAlts, key)
		} else if v == max {
			bestAlts = append(bestAlts, key)
		}
	}
	return
}

// renvoie les meilleures alternatives pour un décompte donné
func minCount(count Count) (bestAlts []Alternative) {
	max := 0
	for key, v := range count {
		if v < max {
			max = v
			bestAlts = make([]Alternative, 0) // On remets à 0
			bestAlts = append(bestAlts, key)
		} else if v == max {
			bestAlts = append(bestAlts, key)
		}
	}
	return
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
func checkProfile(prefs Profile) error {
	cpt := make(map[Alternative]int)
	for _, pref := range prefs {
		for _, alt := range pref {
			cpt[alt] += 1
		}

		last_val := -1
		for _, val := range cpt {
			if val != last_val && last_val != -1 {
				return errors.New("Invalid profile")
			}
			last_val = val
		}
	}
	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	cpt := make(map[Alternative]int)
	for _, pref := range prefs {
		for _, alt := range pref {
			cpt[alt] += 1
		}

		if !sameSlice(alts, pref) {
			return errors.New("Invalid profile")
		}

		last_val := -1
		for _, val := range cpt {
			if val != last_val && last_val != -1 {
				return errors.New("Invalid profile")
			}
			last_val = val
		}
	}
	return nil
}

// vérifie si deux slices ont les mêmes éléments
func sameSlice(x, y []Alternative) bool {
	if len(x) != len(y) {
		return false
	}

	diff := make(map[Alternative]int, len(x))
	for _, xx := range x {
		diff[xx]++
	}

	for _, yy := range y {
		// Si l'alternative _y n'est pas dans diff, on sort de la boucle
		if _, ok := diff[yy]; !ok {
			return false
		}
		diff[yy] -= 1
		if diff[yy] == 0 {
			delete(diff, yy)
		}
	}

	return len(diff) == 0
}
