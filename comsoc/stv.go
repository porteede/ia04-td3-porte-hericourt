package comsoc

func STV_SWF(p Profile) (count Count, err error) {
	// Ici, le candidat le plus mauvais sera celui avec le plus grand score, afin d'utiliser maxCount pour identifier le candidat à éliminer.
	count = make(Count)
	classement := 0
	err = nil
	var bestAlt Alternative
	alternativeList := p[0]
	for len(alternativeList) > 1 { // Tant qu'on a pas de gagnant
		tempCount := make(Count)
		for _, v1 := range p {
			for _, v2 := range v1 { // On cherche la seule valeur à ne pas incrémenter, c'est à dire le candidat voté par v1
				if rank(v2, alternativeList) != -1 {
					bestAlt = v2
					break
				}
			}
			for _, v3 := range alternativeList { // On ajoute 1 à chaque alternative non votée par v1
				if v3 != bestAlt {
					tempCount[v3]++
				}
			}
		}
		// On teste si un candidat a eu la majorité des votes :
		// for a,v4 : range tempCount{
		// 	if v4 < len(p)/2
		// }
		for _, v5 := range maxCount(tempCount) {
			alternativeList = remove(alternativeList, v5)
			count[v5] += classement
			classement++
		}
	}
	count[alternativeList[0]] += classement // On donne le plus gros score au gagnant final
	return
}

func remove(s []Alternative, a Alternative) []Alternative { // Permets de supprimer un élément d'un slice d'alternatives
	i := rank(a, s)
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	var c Count
	c, err = CopelandSWF(p)
	if err != nil {

	}
	bestAlts = maxCount(c)
	return
}
