package agt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	cs "gitlab.utc.fr/porteede/ia04-td3-porte-hericourt/comsoc"
)

// structure de l'agent
type VoterAgent struct {
	AgentI
	id         string
	prefs      []cs.Alternative
	url        string
	channel    chan string
	thresholds []int
}

// constructeur
func NewVoterAgent(id string, prefs []cs.Alternative, url string, channel chan string, thresholds []int) *VoterAgent {
	return &VoterAgent{id: id, prefs: prefs, url: url, channel: channel, thresholds: thresholds}
}

// =======================================================
// méthodes de l'API
func (a *VoterAgent) doVote(ballot_id string) (res string, err error) {
	// creation de la requete
	req := RequestVote{
		VoterId:  a.id,
		BallotId: ballot_id,
		Prefs:    a.prefs,
		Options:  a.thresholds,
	}

	// sérialisation de la requête
	url := a.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	res = fmt.Sprintf("code réponse %d", resp.StatusCode)
	return
}

// =======================================================
// initialisation et démarrage de l'agent
func (a *VoterAgent) Start() {
	log.Printf("démarrage de %v", a.id)

	for {
		ballot_id := <-a.channel
		if ballot_id == "stop" {
			return
		}

		res, err := a.doVote(ballot_id)

		if err != nil {
			log.Println(a.id, "error:", err.Error())
		} else {
			//log.Printf("[%v] %v %s\n", a.id, a.prefs, res)
			log.Printf("[%v] a voté avec des préférences aléatoires. %s", a.id, res)
		}
	}
}
