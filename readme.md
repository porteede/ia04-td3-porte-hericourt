# Serveur de vote
Auteurs : Gwendal HERICOURT, Eden PORTE

## Lancement
```shell
go install gitlab.utc.fr/porteede/ia04-td3-porte-hericourt@v1.0.0
cd $HOME/go/pkg/gitlab.utc.fr/porteede/ia04-td3-porte-hericourt@v1.0.0
go run cmd/launch/launch-all-agents.go
```

La commande lance le serveur de vote en local sur le port `8080` et créer `arg0` agents clients (10 par défaut) avec des préférences aléatoires sur le nombre maximum de candidats `arg1` (100 par défaut). Lorsque nous créons un vote avec par exemple 6 alternatives, nous modifions les préférences des votants pour qu'elles ne contiennent plus que les préférences concernant les 3 candidats. Pour tester une méthode de vote avec un profil précis il faut voter manuellement via par exemple l'extension firefox REST CLIENT

#### API
Le serveur REST a 3 routes possibles sur `http://localhost:8080` :
* `/new_ballot` qui permet à l'utilisateur de créer une nouvelle élection en précisant la règle de vote, la date limite, les votants qui ne peuvent pas participer, le nombre d'alternatives et un éventuel *tiebreak*.
![](new_ballot.png)
* `/vote` permettant à un agent ou utilisateur de donner ses préférences au `ballotagent` pour une élection donnée. Le champ Options permet de donner le nombre de de candidats pour qui le votant souhaite voter en cas d'approval voting
![](vote.png)
* `/result` pour récupérer le résultat d'une élection donnée si elle est terminée.
![](result.png)

#### Utilisation
Une fois le serveur lancé, l'utilisateur peut créer de nouvelles élections en envoyant une requête JSON à `/new_ballot`. Pour lancer un vote par les agents clients, il suffit de taper l'identifiant de l'élection renvoyé par le serveur dans le terminal.

Tous les agents tenterons de voter pour l'élection donnés. Il ne reste plus qu'à observer le résultat avec `/result` une fois la date limite passée.

## Travail réalisé
Arborescence du serveur de vote
```
ia04-td3-porte-hericourt
├── agt
│   ├── agt.go
│   ├── ballotagent.go
│   └── voteragent.go
├── cmd
│   └── launch
│       └── launch-all-agents.go
├── comsoc
│   ├── approval.go
│   ├── borda.go
│   ├── condorcetWinner.go
│   ├── copeland.go
│   ├── majority.go
│   ├── stv.go
│   ├── tiebreak.go
│   └── utils.go
├── go.mod
├── go.sum
└── readme.md
```

