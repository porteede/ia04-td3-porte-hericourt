package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	bestAlts = make([]Alternative, 0) // On remets à 0
	err = nil
	b := false                // passe à true quand on sait que l'alternative i n'est pas le gagnant de Condorcet
	var ci, cj int            // compteurs pour les alternatives i et j
	for _, v1 := range p[0] { // Pour chaque alternative
		for _, v2 := range p[0] { // Pour chaque alternative
			if v1 != v2 {
				ci = 0
				cj = 0
				for _, v3 := range p {
					if isPref(Alternative(v1), Alternative(v2), v3) {
						ci++
					} else {
						cj++
					}
				}
				if ci < cj {
					b = true
					break // On arrête l'itération courante de la boucle
				}
			}
		}
		if b == true {
			b = false
		} else {
			bestAlts = append(bestAlts, Alternative(v1))
		}
	}
	return
}
