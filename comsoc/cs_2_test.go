package comsoc

import "testing"

func TestCheck(t *testing.T) {
	profile1 := [][]Alternative{
		{1, 2, 3},
		{1, 2, 3},
		{3, 2, 1},
	}
	profile2 := [][]Alternative{
		{1, 2, 3},
		{1, 2},
		{3, 2, 1},
	}

	alts := []Alternative{1, 2, 3}

	res1 := checkProfile(profile1)
	res2 := checkProfile(profile2)

	res3 := checkProfileAlternative(profile1, alts)
	res4 := checkProfileAlternative(profile2, alts)

	if res1 != nil {
		t.Errorf("error, result for profile1 should be nil, %d computed", res1)
	}
	if res2 == nil {
		t.Errorf("error, result for profile2 should be error, %d computed", res2)
	}
	if res3 != nil {
		t.Errorf("error, result for profile1 should be nil, %d computed", res3)
	}
	if res4 == nil {
		t.Errorf("error, result for profile2 should be error, %d computed", res4)
	}
}
