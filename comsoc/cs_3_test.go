package comsoc

import "testing"

func profileAdd(p Profile, nb int, order []Alternative) Profile { // Permet de générer des profils de tests plus facilement
	for i := 0; i < nb; i++ {
		p = append(p, order)
	}
	return p
}

func TestCopelandSWF(t *testing.T) {
	var p Profile // Exemple pris du cours page 35
	p = profileAdd(p, 5, []Alternative{0, 1, 2, 3})
	p = profileAdd(p, 4, []Alternative{1, 2, 3, 0})
	p = profileAdd(p, 3, []Alternative{3, 2, 0, 1})

	res, _ := CopelandSWF(p)

	if res[0] != -1 {
		t.Errorf("error, result for 0 should be -1, %d computed", res[0])
	}
	if res[1] != 1 {
		t.Errorf("error, result for 1 should be 1, %d computed", res[1])
	}
	if res[2] != 1 {
		t.Errorf("error, result for 2 should be 1, %d computed", res[2])
	}
	if res[3] != -1 {
		t.Errorf("error, result for 3 should be -1, %d computed", res[3])
	}
}

func TestSTV(t *testing.T) {
	var p Profile // Exemple pris du cours page 35
	p = profileAdd(p, 5, []Alternative{0, 1, 2, 3})
	p = profileAdd(p, 4, []Alternative{1, 2, 3, 0})
	p = profileAdd(p, 3, []Alternative{3, 2, 0, 1})

	res, _ := STV_SWF(p)

	if res[0] != 3 {
		t.Errorf("error, result for 0 should be -3, %d computed", res[0])
	}
	if res[1] != 2 {
		t.Errorf("error, result for 1 should be 2, %d computed", res[1])
	}
	if res[2] != 0 {
		t.Errorf("error, result for 2 should be 0, %d computed", res[2])
	}
	if res[3] != 1 {
		t.Errorf("error, result for 3 should be 1, %d computed", res[3])
	}
}
