package agt

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"

	cs "gitlab.utc.fr/porteede/ia04-td3-porte-hericourt/comsoc"
)

const LONG_DATE string = "Mon Jan 2 15:04:05 UTC 2006"

// structure de l'agent
type Ballot struct {
	id         string
	rule       string
	deadline   string
	voter_ids  []string
	alts       int
	profile    cs.Profile
	tiebreak   []cs.Alternative
	winner     cs.Alternative
	ranking    []cs.Alternative
	thresholds []int
}

type BallotAgent struct {
	AgentI
	sync.Mutex
	id      string
	addr    string
	ballots []Ballot
}

// constructeur
func NewBallotAgent(id string, addr string) *BallotAgent {
	return &BallotAgent{id: id, addr: addr}
}

// =======================================================
// Utilitaires
func (a *BallotAgent) findBallot(id string) *Ballot {
	for i, b := range a.ballots {
		if b.id == id {
			return &a.ballots[i]
		}
	}
	return nil
}

func (a *BallotAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method %q not allowed", r.Method)
		return false
	}
	return true
}

func (b *Ballot) resolveBallot() (err error) {
	var swf func(cs.Profile) (cs.Count, error)
	switch b.rule {
	case "borda":
		swf = cs.BordaSWF
	case "copeland":
		swf = cs.CopelandSWF
	case "stv":
		swf = cs.STV_SWF
	case "approval":
		var count cs.Count
		count, err = cs.ApprovalSWF(b.profile, b.thresholds)
		alts := make([]cs.Alternative, 0, len(count))
		for key := range count {
			alts = append(alts, key)
		}
		sort.SliceStable(alts, func(i, j int) bool {
			return count[alts[i]] > count[alts[j]]
		})
		b.ranking = alts
		if err == nil && len(b.ranking) > 0 {
			b.winner = b.ranking[0]
		}
		return
	default:
		swf = cs.MajoritySWF
	}
	f := cs.SWFFactory(swf, cs.TieBreakFactory(b.tiebreak))
	b.ranking, err = f(b.profile)
	if err == nil && len(b.ranking) > 0 {
		b.winner = b.ranking[0]
	}
	return
}

func decodeRequest[T interface{}](r *http.Request) (req T, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func trimPreferences(b Ballot, prefs []cs.Alternative) []cs.Alternative {
	trimed_prefs := make([]cs.Alternative, 0)
	for _, v := range prefs {
		if int(v) <= b.alts {
			trimed_prefs = append(trimed_prefs, v)
		}
	}
	return trimed_prefs
}

// =======================================================
// méthodes de l'API
func (a *BallotAgent) doNewBallot(w http.ResponseWriter, r *http.Request) {
	// vérification de la méthode de la requête
	if !a.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[RequestNewBallot](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	new_ballot := Ballot{
		id:        fmt.Sprintf("ballot%d", len(a.ballots)),
		rule:      strings.ToLower(req.Rule),
		deadline:  req.Deadline,
		voter_ids: req.VoterIds,
		alts:      req.Alts,
		tiebreak:  req.Tiebreak,
	}

	// si aucun tiebreak n'a été rensigné, on en génère un par défaut
	if len(new_ballot.tiebreak) <= 0 {
		// alternatives de "1" à "new_ballot.alts"
		for i := 1; i <= new_ballot.alts; i++ {
			new_ballot.tiebreak = append(new_ballot.tiebreak, cs.Alternative(i))
		}
	}

	a.Lock()
	a.ballots = append(a.ballots, new_ballot)
	a.Unlock()
	log.Printf("création du ballot %s", new_ballot.id)

	// envoi de la réponse
	resp := ResponseNewBallot{new_ballot.id}
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
	return
}

func (a *BallotAgent) doVote(w http.ResponseWriter, r *http.Request) {
	// vérification de la méthode de la requête
	if !a.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[RequestVote](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// recuperation du ballot demandé
	ballot := a.findBallot(req.BallotId)
	if ballot == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// verification de la deadline
	deadline, _ := time.Parse(LONG_DATE, ballot.deadline)
	now, _ := time.Parse(LONG_DATE, time.Now().Format(LONG_DATE))
	if now.After(deadline) {
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}

	// verification si le votant a déjà voté
	for _, agent_id := range ballot.voter_ids {
		if agent_id == req.VoterId {
			w.WriteHeader(http.StatusForbidden)
			return
		}
	}

	a.Lock()
	// traitement de la requête
	ballot.profile = append(ballot.profile, trimPreferences(*ballot, req.Prefs))
	ballot.voter_ids = append(ballot.voter_ids, req.VoterId)
	if ballot.rule == "approval" {
		ballot.thresholds = append(ballot.thresholds, req.Options[0])
	}
	a.Unlock()
	w.WriteHeader(http.StatusOK)
}

func (a *BallotAgent) doResult(w http.ResponseWriter, r *http.Request) {
	// vérification de la méthode de la requête
	if !a.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := decodeRequest[RequestResult](r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// recuperation du ballot demandé
	ballot := a.findBallot(req.BallotId)
	if ballot == nil {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	// verification de la deadline
	deadline, _ := time.Parse(LONG_DATE, ballot.deadline)
	now, _ := time.Parse(LONG_DATE, time.Now().Format(LONG_DATE))
	if deadline.After(now) {
		w.WriteHeader(http.StatusTooEarly)
		return
	}

	// traitement de la requete
	if len(ballot.ranking) <= 0 {
		a.Lock()
		ballot.resolveBallot()
		a.Unlock()
	}

	// envoi de la réponse
	resp := ResponseResult{ballot.winner, ballot.ranking}
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

// =======================================================
// initialisation et démarrage de l'agent
func (a *BallotAgent) Start() {
	// création du multiplexer
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", a.doNewBallot)
	mux.HandleFunc("/vote", a.doVote)
	mux.HandleFunc("/result", a.doResult)

	// création du serveur http
	s := &http.Server{
		Addr:           a.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", a.addr)
	go log.Fatal(s.ListenAndServe())
}
