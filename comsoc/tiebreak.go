package comsoc

import (
	"errors"
	"sort"
)

// Prototype d'une fonction TieBeak()
// func TieBreak([]Alternative) (Alternative, err error)

// Fonction de génération de fonction de TieBreak()
func TieBreakFactory(ref_alts []Alternative) func([]Alternative) (Alternative, error) {
	return func(alts []Alternative) (alt Alternative, err error) {
		if len(alts) <= 0 {
			err = errors.New("No alternatives")
			return
		}

		idx := make(map[Alternative]int)
		for i, v := range ref_alts {
			idx[v] = i
		}

		alt = alts[0]
		for i, key := range alts {
			val, ok := idx[key]
			if ok && i >= val {
				alt = key
			}
		}
		return
	}
}

// Fonction de génération de SWF sans égalité
func SWFFactory(swf func(Profile) (Count, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile) ([]Alternative, error) {
	return func(p Profile) (alts []Alternative, err error) {
		count, err := swf(p)
		if err != nil {
			return
		}

		// récupération de la liste des alternatives dans l'ordre des préférences
		alts = make([]Alternative, 0, len(count))
		for key := range count {
			alts = append(alts, key)
		}
		sort.SliceStable(alts, func(i, j int) bool {
			if count[alts[i]] == count[alts[j]] {
				alt, _ := tiebreak([]Alternative{alts[i], alts[j]})
				return alt == alts[i]
			} else {
				return count[alts[i]] > count[alts[j]]
			}
		})

		// tri des alternatives en fonction du Tiebreak
		// sort.SliceStable(alts, func(i, j int) bool {
		// 	alt, _ := tiebreak([]Alternative{alts[i], alts[j]})
		// 	return alt == alts[i]
		// })

		return
	}
}

// Fonction de génération de SCF sans égalité
func SCFFactory(scf func(Profile) ([]Alternative, error), tiebreak func([]Alternative) (Alternative, error)) func(Profile) (Alternative, error) {
	return func(p Profile) (alt Alternative, err error) {
		alts, err := scf(p)
		if err != nil {
			return
		}

		alt, err = tiebreak(alts)
		if err != nil {
			return
		}

		return
	}
}
